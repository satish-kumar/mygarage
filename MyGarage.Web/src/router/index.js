import Vue from 'vue'
import Router from 'vue-router'
import Dashboard from "@/components/Dashboard.vue"
import VehicleDetail from "@/components/VehicleDetail.vue"
import EditVehicle from "@/components/EditVehicle.vue"
import ServiceOffers from "@/components/ServiceOffers.vue"
import TradeInOffers from "@/components/TradeInOffers.vue"
import MembersList from "@/components/MembersList.vue"
import EditMember from "@/components/EditMember.vue"

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '*',
      name: 'dashboard',
      component: Dashboard
    },
    {
      path: '/vehicledetail',
      name: 'vehicledetail',
      component: VehicleDetail
    },
    {
      path: '/editvehicle',
      name: 'editvehicle',
      component: EditVehicle
    },
    {
      path: '/serviceoffers',
      name: 'serviceoffers',
      component: ServiceOffers
    },
    {
      path: '/tradeinoffers',
      name: 'tradeinoffers',
      component: TradeInOffers
    },
    {
      path: '/memberslist',
      name: 'memberslist',
      component: MembersList
    },
    {
      path: '/editmember',
      name: 'editmember',
      component: EditMember
    }
  ]
})
