import Vue from "vue";

export const CloudinaryClient = {
  openImageUploader: function (presetType, folder, fileTypes, multiple) {
    return new Promise((resolve, reject) => {
      try {
        cloudinary.openUploadWidget({
            cloud_name: "mygarage",
            upload_preset: presetType,
            folder: folder,
            multiple: multiple,
            client_allowed_formats: fileTypes
          },
          function (error, result) {
            resolve({
              Error: error,
              Result: result
            });
          }
        );
      } catch (err) {
        reject(err);
      }
    });
  }
}
