﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MyGarageAPI.Models
{
    public class OfferModels
    {
        public string OfferImage { get; set; }
        public string OfferModel { get; set; }
        public string OfferPayment { get; set; }
    }
}