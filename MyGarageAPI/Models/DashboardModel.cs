﻿using Data.ES;
using MyGarageAPI.Models;
using System.Collections.Generic;

namespace MyGarageAPI.Controllers
{
    public class DashboardModel
    {
        public PrimaryPerson User { get; set; }
        public List<VehicleModel> VehicleList { get; set; }
    }
}