﻿using Data.ES;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MyGarageAPI.Models
{
    public class VehicleModel : Vehicle
    {

        public string NickName { get; set; }
        public string Make { get; set; }
        public string Model { get; set; }
        public int Year { get; set; }
        public string LeanHolder { get; set; }
        public double MonthlyPayment { get; set; }
        public double PayoffAmmount { get; set; }
        public string VehiclePictureUrl { get; set; }
        public int Mileage { get; set; }
        public string VIN { get; set; }
        public bool Primary { get; set; }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string PreferedProvider { get; set; }
        public string ServiceManager { get; set; }
        public string LastServiceDate { get; set; }
        public string ServiceType { get; set; }
        public string Style { get; set; }
        public string LastPaymentDate { get; set; }
        public string NextServiceDue { get; set; }
        public string YearToDateServicePaid { get; set; }
        public string CurrentMPG { get; set; }
        public string OriginalMPG { get; set; }
        public Guid VehicleID { get; set; }
        public Guid PersonID { get; set; }
        public Guid HouseID { get; set; }
        public string InsuranceCompany { get; set; }
        public double InsurancePayment { get; set; }
        public string InsuraceCardUrl { get; set; }
        public List<OfferModels> Offers { get; set; }



    }
}