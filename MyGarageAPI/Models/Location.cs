﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MyGarageAPI.Models
{
    public class Location
    {
        public string Name { get; set; }

        // in Meters
        public int Distance { get; set; }

        public string StreetAdress { get; set; }

        public string City { get; set; }

        public string State{ get; set; }

        public string Country { get; set; }

        public string PostalCode { get; set; }

    }
}