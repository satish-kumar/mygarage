﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MyGarageAPI.Models
{
    public class FoursquareRequestModel
    {
        public string ClientId { get; set; }

        public string ClientSecret { get; set; }

        public int ResultLimit { get; set; }

        public float Latitude { get; set; }

        public float Longitude { get; set; }

        // pass in as YYYYMMDD to bring in API changes up to this date, needs validation
        public string Version { get; set; }

        public string CarMake { get; set; }
    }
}