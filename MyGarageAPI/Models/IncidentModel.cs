﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MyGarageAPI.Enums.Incident;

namespace MyGarageAPI.Models
{
    public class IncidentModel
    {
        //an Incident is an event or occurrence, most notable accidents or tickets
        public DateTime IncidentDate { get; set; }

        //public string Location { get; set; }

        public Incident IncidentType { get; set; }

        public string Notes { get; set; }



    }
}