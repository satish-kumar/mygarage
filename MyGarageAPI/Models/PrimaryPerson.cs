﻿using Data.ES;
using MyGarageAPI.Models;

namespace MyGarageAPI.Controllers
{
    public class PrimaryPerson
    {
        public PersonModel Person { get; set; }
        public VehicleModel Vehicle { get; set; }
        public InsuranceModel Insurance { get; set; }
    }
}