﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MyGarageAPI.Models
{
    public class InsuranceModel
    {

        public string InsuranceProvider { get; set; }
        public string InsurancePolicyNo { get; set; }
        public string InsuranceCardPictureUrl { get; set; }
        public string AgentPhone { get; set; }
        public int Accidents { get; set; }



        public Guid VehicleID { get; set; }
        public Guid PersonID { get; set; }

        public Guid HouseID { get; set; }
        public Guid InsuranceID { get; set; }
    }
}