﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MyGarageAPI.Models
{
    public class PersonModel
    {
        public PersonModel()
        {
            
        }


        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string ProfilePictureUrl { get; set; }
        public string Role { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Address { get; set; }

        public Guid PersonID { get; set; }
        public Guid HouseID { get; set; }

    }
}