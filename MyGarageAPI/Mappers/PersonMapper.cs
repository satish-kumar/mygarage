﻿using AutoMapper;
using Data.ES;
using MyGarageAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MyGarageAPI.Mappers
{
    public class PersonMapper : Profile
    {
        public PersonMapper()
        {
            CreateMap<Person.Data, PersonModel>();
            CreateMap<PersonModel, Person.Data>();
            CreateMap<VehicleModel, Vehicle.Data>();
            CreateMap<Vehicle.Data, VehicleModel>();
            CreateMap<Insurance.Data, InsuranceModel>();
            CreateMap<InsuranceModel, Insurance.Data>();
            CreateMap<Insurance.Data, VehicleModel>();
            CreateMap<VehicleModel, Insurance.Data>();
        }
    }
}