﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MyGarageAPI.Enums
{
    public enum ESType
    {
        person,
        vehicle,
        insurance,
        financial
    }
}