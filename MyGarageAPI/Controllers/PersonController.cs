﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MyGarageAPI.Models;
using MyGarageAPI.Providers;
using MyGarageAPI.Results;
using System.Web.Http;
using Data.ES;
using AutoMapper;
using MyGarageAPI.Mappers;
using System.Threading.Tasks;

namespace MyGarageAPI.Controllers
{
    //[Authorize]
    //[RoutePrefix("api/Person")]
    public class PersonController : ApiController
    {
        //POST api/Person/CreatePerson
        public async Task<IHttpActionResult> CreatePerson(PersonModel person)
        {
            //PersonModel newPerson = new PersonModel();
            //if this successfully creates a person
            return Ok();
        }


        [HttpGet]
        [Route("api/Person/Upsert")]
        public void Upsert()
        {
            Person es = new Person(true);
            Vehicle es2 = new Vehicle(true);
            Offer es3 = new Offer(true);
            Insurance es4 = new Insurance(true);
        }
        [HttpGet]
        [Route("api/Person/InsertTest")]
        public void TestInsert()
        {
            Person es1 = new Person();
            Guid houseId = Guid.NewGuid();
            Guid personId = Guid.NewGuid();
            Guid vehicleId = Guid.NewGuid();
            Person.Data person = new Person.Data();
            person.FirstName = "Joe";
            person.LastName = "Blow";
            person.ProfilePictureUrl = "testurl.com";
            person.PersonID = personId;
            person.HouseID = houseId;
            es1.Save(person);

            Vehicle es = new Vehicle();

            Vehicle.Data vehicle = new Vehicle.Data();
            vehicle.HouseID = houseId;
            vehicle.LeanHolder = "Capital One";
            vehicle.Make = "Ford";
            vehicle.Model = "Fusion";
            vehicle.Mileage = 25324;
            vehicle.MonthlyPayment = 425.50;
            vehicle.NickName = "Joe's Fusion";
            vehicle.PayoffAmmount = 22500.50;
            vehicle.VehicleID = Guid.NewGuid();
            vehicle.VehiclePictureUrl = "testurl.com";
            vehicle.VIN = "2Z432GSR785RG7382";
            vehicle.Year = 2016;
            vehicle.PersonID = personId;
            vehicle.FirstName = person.FirstName;
            vehicle.LastName = person.LastName;
            vehicle.Email = person.Email;

            es.Save(vehicle);

            Vehicle.Data vehicle2 = new Vehicle.Data();
            vehicle.HouseID = houseId;
            vehicle.LeanHolder = "Capital One";
            vehicle.Make = "Ford";
            vehicle.Model = "F-150";
            vehicle.Mileage = 65324;
            vehicle.MonthlyPayment = 325.50;
            vehicle.NickName = "Joe's F-150";
            vehicle.PayoffAmmount = 9500.50;
            vehicle.VehicleID = vehicleId;
            vehicle.VehiclePictureUrl = "testurl.com";
            vehicle.VIN = "2Z432GSR785RG7382";
            vehicle.Year = 2013;
            vehicle.PersonID = personId;
            vehicle.FirstName = person.FirstName;
            vehicle.LastName = person.LastName;
            vehicle.Email = person.Email;
            vehicle.Primary = true;

            es.Save(vehicle);
            Insurance es2 = new Insurance();
            Insurance.Data insurance = new Insurance.Data();
            insurance.Accidents = 0;
            insurance.AgentPhone = "913-555-5555";
            insurance.HouseID = houseId;
            insurance.InsurancePolicyNo = "12345";
            insurance.InsuranceCompany = "Allstate";
            insurance.PersonID = personId;
            insurance.VehicleID = vehicleId;
            insurance.InsuranceID = Guid.NewGuid();

            es2.Save(insurance);

        }

        [HttpGet]
        [Route("api/Person/GetPersonList/{houseId}")]
        public List<Person.Data> GetPersonList(Guid houseId)
        {
            Person es1 = new Person();
            var personList = es1.GetByHouseId(houseId);
            return personList;
        }

        [HttpGet]
        [Route("api/Person/GetDashboardData/{personId}")]
        public IHttpActionResult GetDashboardData(Guid personId)
        {
            DashboardModel retrunObject = new DashboardModel();
            Person personES = new Person();
            Vehicle vehicleES = new Vehicle();
            Insurance insuranceES = new Insurance();
            Mapper.Reset();
            try
            {
                Mapper.Initialize(cfg =>
                {
                    cfg.AddProfile<PersonMapper>();
                });
            }
            catch (Exception ex)
            {

            }
            retrunObject.User = new PrimaryPerson();
            retrunObject.User.Person = Mapper.Map<PersonModel>(personES.GetByPersonId(personId));
            retrunObject.User.Vehicle = Mapper.Map<VehicleModel>(vehicleES.GetPrimaryByPersonId(personId));
            retrunObject.User.Insurance = Mapper.Map<InsuranceModel>(retrunObject.User.Vehicle != null ? insuranceES.GetByVehicleId(retrunObject.User.Vehicle.VehicleID).FirstOrDefault() : null);
            retrunObject.VehicleList = Mapper.Map<List<VehicleModel>>(vehicleES.GetByHouseId(retrunObject.User.Person.HouseID));

            return Ok(retrunObject);
        }

        [HttpPost]
        [Route("api/Person/Add")]
        public IHttpActionResult AddPerson(PersonModel person)
        {
            Person es = new Person();
            Person.Data esPerson = new Person.Data();

            try
            {
                Mapper.Initialize(cfg =>
                {
                    cfg.AddProfile<PersonMapper>();
                });
            }
            catch (Exception ex)
            {

            }

            esPerson = Mapper.Map<Person.Data>(person);
            if (person.HouseID == new Guid())
            {
                person.HouseID = Guid.NewGuid();
                esPerson.HouseID = person.HouseID;
            }
            if (person.PersonID == new Guid())
            {
                esPerson.PersonID = Guid.NewGuid();
                person.PersonID = esPerson.PersonID;
            }

            try
            {
                es.Save(esPerson);

            }
            catch
            {
                return InternalServerError();
            }

            return Ok(person);
        }

        [HttpGet]
        [Route("api/Person/GetPerson/{personId}")]
        public IHttpActionResult GetPerson(Guid personId)
        {
            Person es1 = new Person();
            var person = es1.GetByPersonId(personId);
            try
            {
                Mapper.Initialize(cfg =>
                {
                    cfg.AddProfile<PersonMapper>();
                });
            }
            catch (Exception ex)
            {

            }
            PersonModel returnPerson = new PersonModel();

            if (person != null)
                returnPerson = Mapper.Map<PersonModel>(person);

            return Ok(returnPerson);
        }

    }
}