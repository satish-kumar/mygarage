﻿using MyGarageAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Helpers;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using System.Net;
using System.IO;

namespace MyGarageAPI.Controllers
{
    public class OfferController : ApiController
    {
        // GET api/Offer/GetLocations
        [HttpPost]
        [Route("api/offer/GetLocation")]
        public List<Location> GetLocations(FoursquareRequestModel locationsRequest)
        {
            string queryURL =
                "https://api.foursquare.com/v2/venues/search?ll=LAT,LNG&client_id=OLO5RFYJCFSCFAAHIWTMBGCCVGTXRD1EVFYQZODSIDAZHAGB&client_secret=T4YH4T3U5VYOKCSAGKWOHOS4B0K40YBYBT2X1BAGIRHJRMKN&v=20180722&query=MAKE&limit=5";

            string lat = locationsRequest.Latitude.ToString();
            string lng = locationsRequest.Longitude.ToString();
            string searchWord = locationsRequest.CarMake;

            queryURL.Replace("LAT", lat).Replace("LNG", lng).Replace("MAKE", searchWord);

            Uri address = new Uri(queryURL);

            HttpWebRequest request = WebRequest.Create(address) as HttpWebRequest;

            request.Method = "GET";

            request.ContentType = "text/json";

            List<Location> locations = new List<Location>();


            using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
            {
                StreamReader reader = new StreamReader(response.GetResponseStream());
                string output = reader.ReadToEnd();

                //Location returnedLocation = new Location();

                locations = JsonConvert.DeserializeObject<List<Location>>(output);

                //returnedLocation.City = deserializedLocation.City;
            }

            return locations;
        }
    }
}