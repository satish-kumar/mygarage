﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MyGarageAPI.Models;
using MyGarageAPI.Providers;
using MyGarageAPI.Results;
using System.Web.Http;
using Data.ES;
using AutoMapper;
using MyGarageAPI.Mappers;

namespace MyGarageAPI.Controllers
{
    public class VehicleController : ApiController
    {
        [HttpGet]
        [Route("api/Vehicle/Upsert")]
        public void Upsert()
        {
            Vehicle es = new Vehicle(true);
        }

        [HttpGet]
        [Route("api/Vehicle/GetVehicle/{vehicleId}")]
        public IHttpActionResult GetVehicle(Guid vehicleId)
        {
            Vehicle es1 = new Vehicle();
            Insurance es2 = new Insurance();
            Offer es3 = new Offer();

            var vehicle = es1.GetByVehicleId(vehicleId);
            var insurance = es2.GetByVehicleId(vehicleId).FirstOrDefault();
            var offers = es3.GetByVehicleId(vehicleId);
            Mapper.Reset();
            Mapper.Initialize(cfg => { cfg.AddProfile<PersonMapper>(); });
            VehicleModel returnVehicle = new VehicleModel();

            if (vehicle != null)
            {
                returnVehicle = Mapper.Map<VehicleModel>(vehicle);
                if(insurance != null)
                {
                    returnVehicle.InsuranceCompany = insurance.InsuranceCompany;
                    returnVehicle.InsuraceCardUrl = insurance.InsuranceCardUrl;
                    returnVehicle.InsurancePayment = insurance.InsurancePayment;
                }

                if(offers != null)
                returnVehicle.Offers = Mapper.Map<List<OfferModels>>(offers);
            }

            return Ok(returnVehicle);
        }

        [HttpPost]
        [Route("api/Vehicle/Add")]
        public IHttpActionResult AddPerson(VehicleModel vehicle)
        {
            Vehicle es = new Vehicle();
            Vehicle.Data esVehicle = new Vehicle.Data();
            Insurance es1 = new Insurance();
            Insurance.Data esInsurance = new Insurance.Data();
            Offer es2 = new Offer();
            Offer.Data esOffer1 = new Offer.Data();
            esOffer1.OfferImage = "http://cars.typepad.com/.a/6a00d83451b3c669e201b7c8c517ce970b-pi";
            esOffer1.OfferModel = vehicle.Model;
            esOffer1.OfferPayment = "$50";
            esOffer1.VehicleID = vehicle.VehicleID;

            Offer.Data esOffer2 = new Offer.Data();
            esOffer2.OfferImage = "https://st.motortrend.com/uploads/sites/5/2018/06/2018-Ford-Mustang-GT-front-three-quarter-in-motion-02.jpg?interpolation=lanczos-none&fit=around|660:438";
            esOffer2.OfferModel = vehicle.Model;
            esOffer2.OfferPayment = "$75";
            esOffer2.VehicleID = vehicle.VehicleID;

            Mapper.Reset();
            Mapper.Initialize(cfg => {
                cfg.AddProfile<PersonMapper>();
            });

            esVehicle = Mapper.Map<Vehicle.Data>(vehicle);
            esVehicle.VehicleID = Guid.NewGuid();
            vehicle.VehicleID = esVehicle.VehicleID;
            esInsurance = Mapper.Map<Insurance.Data>(vehicle);
            try
            {
                es.Save(esVehicle);
                es1.Save(esInsurance);
                es2.Save(esOffer1);
                es2.Save(esOffer2);

            }
            catch
            {
                return InternalServerError();
            }

            return Ok(vehicle);
        }
    }
}