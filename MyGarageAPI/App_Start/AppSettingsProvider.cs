﻿using System.Configuration;

namespace MyGarageAPI.App_Start
{
    public class AppSettingsProvider : IConfigurationProvider
    {
        public void Dispose()
        {
        }

        public string Get(string key)
        {
            return ConfigurationManager.AppSettings[key];
        }

        public void Set(string key, string value)
        {
            ConfigurationManager.AppSettings.Set(key, value);
        }
    }
}