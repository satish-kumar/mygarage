﻿using CommonServiceLocator;
using CommonServiceLocator.NinjectAdapter.Unofficial;
using Microsoft.Practices.ServiceLocation;
using MyGarageAPI.Areas.HelpPage;
using Ninject;
using Ninject.Web.Common;
using Ninject.Web.WebApi.Filter;
using Ninject.Extensions.Conventions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.Validation;

namespace MyGarageAPI.App_Start
{
    public static class NinjectConfig
    {
        private static readonly Bootstrapper bootstrapper = new Bootstrapper();

        public static IKernel CreateKernel()
        {
            var kernel = new StandardKernel();
            try
            {
                kernel.Load(
                    "Data.ES.dll"
                );
                kernel.Bind<Func<IKernel>>().ToMethod(ctx => () => new Bootstrapper().Kernel);

                RegisterServices(kernel);
                return kernel;
            }
            catch
            {
                kernel.Dispose();
                throw;
            }
        }

        private static void RegisterServices(IKernel kernel)
        {
            //ALWAYS EXCLUDE THE BINDINGS FROM THE kernel.Load() call in CreateKernel()
            kernel.Bind(x => x.FromAssembliesMatching("Data.ES"));

            kernel.Rebind<IConfigurationProvider>().To(typeof(AppSettingsProvider));


            kernel.Bind<DefaultModelValidatorProviders>().ToConstant(
                        new DefaultModelValidatorProviders(
                        GlobalConfiguration.Configuration.Services
                        .GetServices(typeof(ModelValidatorProvider))
                        .Cast<ModelValidatorProvider>())
            );
            kernel.Bind<DefaultFilterProviders>()
                .ToConstant(new DefaultFilterProviders(new[] { new NinjectFilterProvider(kernel) }
                .AsEnumerable()));
            kernel.Bind<IDocumentationProvider>().To<XmlDocumentationProvider>().WithConstructorArgument("documentPath", GetXmlCommentsPath());
            ServiceLocator.SetLocatorProvider(() => new NinjectServiceLocator(kernel));
        }

        private static string GetXmlCommentsPath()
        {
            return System.String.Format(@"{0}\bin\MyGarageAPI.XML", System.AppDomain.CurrentDomain.BaseDirectory);
        }
    }
}