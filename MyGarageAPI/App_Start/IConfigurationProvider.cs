﻿using System;

namespace MyGarageAPI.App_Start
{
    public interface IConfigurationProvider : IDisposable
    {
        string Get(string key);
        void Set(string key, string value);
    }
}