﻿using Nest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.ES
{
    public partial class Offer
    {


        private void CreateMapping()
        {
            Client.Map<Data>(m => m.Type(FieldName.IndexTypeName)
                                   .Index(Indices.Index(Index))
                                   .Dynamic(DynamicMapping.Strict)
                                   .DateDetection(true)
                                   .NumericDetection(true)
                                   .AutoMap());
        }

        [ElasticsearchType(Name = FieldName.IndexTypeName, IdProperty = FieldName.Id)]
        [Serializable]
        public partial class Data : CommonData
        {
            [Number(NumberType.Integer, Name = FieldName.OfferType)]                public int OfferType { get; set; }
            [Text(Name = FieldName.Special)]                                        public string Special { get; set; }
            [Number(NumberType.Double, Name = FieldName.AverageServiceCost)]        public string AverageServiceCost { get; set; }
            [Number(NumberType.Integer, Name = FieldName.DistanceToDealer)]         public int DistanceToDealer { get; set; }
            [Number(NumberType.Double, Name = FieldName.TradeInValue)]              public string TradeInValue { get; set; }

            [Text(Name = FieldName.OfferImage)]                                        public string OfferImage { get; set; }
            [Text(Name = FieldName.OfferModel)]                                        public string OfferModel { get; set; }
            [Text(Name = FieldName.OfferPayment)]                                        public string OfferPayment { get; set; }
            [Keyword(Name = FieldName.VehicleID)]                                   public Guid VehicleID { get; set; }


        }
        public class FieldName : FieldNameBase
        {
            public const string IndexTypeName = Constants.Vehicle;
            public const string OfferType = "offerType";
            public const string VehicleID = "vehicleID";
            public const string Special = "special";
            public const string DistanceToDealer = "distanceToDealer";
            public const string AverageServiceCost = "averageServiceCost";
            public const string TradeInValue = "tradeInValue";
            public const string OfferPayment = "offerPayment";
            public const string OfferModel = "offerModel";
            public const string OfferImage = "offerImage";





        }
    }
}
