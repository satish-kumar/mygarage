﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nest;

namespace Data.ES
{
    public partial class Insurance : MyGarageIndex
    {

        #region Constructor
        public Insurance(bool UpsertMapping = false, string IndexNameOverride = null) : base("insurance", IndexNameOverride, Constants.Insurance)
        {
            if (UpsertMapping)
            {
                CreateMapping();
            }
        }

        #endregion

        //#region Public Stuff
        public bool Delete(string Id)
        {
            IDeleteResponse IDeleteResponse = Client.Delete<Data>(Id);

            return ((IDeleteResponse == null) || !IDeleteResponse.ApiCall.Success) ? false : true;
        }

        public Data Get(string Id)
        {
            IGetResponse<Data> GetResponse = Client.Get<Data>(new DocumentPath<Data>(Id));

            return ((GetResponse == null) || !GetResponse.ApiCall.Success || !GetResponse.Found) ? null : GetResponse.Source;
        }

        public Data Save(Data Data)
        {
            //
            // Set Data items
            //
            SetCommons(Data);

            if (String.IsNullOrWhiteSpace(Data.Id)) Data.Id = GenerateId(null);

            //
            // Do actual ES indexing and return the outcome
            //
            Nest.IndexRequest<Data> IndexRequest = new IndexRequest<Data>(Data, "insurace", Constants.Insurance);
            IIndexResponse IndexResponse = Client.Index<Data>(IndexRequest);

            CheckSaveError(Data.Id, IndexResponse);

            return Data;
        }


        public Data GetByInsuranceId(Guid insuranceId)
        {
            ISearchResponse<Data> SearchResponse = null;
            int x = 0;
            do
            {
                try
                {
                    SearchResponse = Client.Search<Data>(s => s.Size(1)
                                                                             .Query(q => q.Bool(b => b.Must(m => m.Term(FieldName.InsuranceID, insuranceId)))));
                }
                catch
                {
                    System.Threading.Thread.Sleep(TimeSpan.FromMilliseconds(new Random().Next(2000, 4000)));
                }

                x++;
            } while (x < 5 && ((SearchResponse == null) || !SearchResponse.ApiCall.Success));
            if ((SearchResponse == null) || !SearchResponse.ApiCall.Success)
            {

                SearchResponse = Client.Search<Data>(s => s.Size(1)
                                                                             .Query(q => q.Bool(b => b.Must(m => m.Term(FieldName.InsuranceID, insuranceId)))));
            }
            return ((SearchResponse == null) || !SearchResponse.ApiCall.Success || (SearchResponse.Documents == null)) ? null : SearchResponse.Documents.FirstOrDefault<Data>();
        }
        public List<Data> GetByVehicleId(Guid vehicleId)
        {
            ISearchResponse<Data> SearchResponse = null;
            int x = 0;
            do
            {
                try
                {
                    SearchResponse = Client.Search<Data>(s => s.Size(1000)
                                                                             .Query(q => q.Bool(b => b.Must(m => m.Term(FieldName.VehicleID, vehicleId)))));
                }
                catch
                {
                    System.Threading.Thread.Sleep(TimeSpan.FromMilliseconds(new Random().Next(2000, 4000)));
                }

                x++;
            } while (x < 5 && ((SearchResponse == null) || !SearchResponse.ApiCall.Success));
            if ((SearchResponse == null) || !SearchResponse.ApiCall.Success)
            {

                SearchResponse = Client.Search<Data>(s => s.Size(1000)
                                                                             .Query(q => q.Bool(b => b.Must(m => m.Term(FieldName.VehicleID, vehicleId)))));
            }
            return ((SearchResponse == null) || !SearchResponse.ApiCall.Success || (SearchResponse.Documents == null)) ? null : SearchResponse.Documents.ToList<Data>();
        }

        public List<Data> GetByPersonId(Guid personId)
        {
            ISearchResponse<Data> SearchResponse = null;
            int x = 0;
            do
            {
                try
                {
                    SearchResponse = Client.Search<Data>(s => s.Size(1000)
                                                                             .Query(q => q.Bool(b => b.Must(m => m.Term(FieldName.PersonID, personId)))));
                }
                catch
                {
                    System.Threading.Thread.Sleep(TimeSpan.FromMilliseconds(new Random().Next(2000, 4000)));
                }

                x++;
            } while (x < 5 && ((SearchResponse == null) || !SearchResponse.ApiCall.Success));
            if ((SearchResponse == null) || !SearchResponse.ApiCall.Success)
            {

                SearchResponse = Client.Search<Data>(s => s.Size(1000)
                                                                             .Query(q => q.Bool(b => b.Must(m => m.Term(FieldName.PersonID, personId)))));
            }
            return ((SearchResponse == null) || !SearchResponse.ApiCall.Success || (SearchResponse.Documents == null)) ? null : SearchResponse.Documents.ToList<Data>();
        }

        public List<Data> GetByHouseId(Guid houseId)
        {
            ISearchResponse<Data> SearchResponse = null;
            int x = 0;
            do
            {
                try
                {
                    SearchResponse = Client.Search<Data>(s => s.Size(1000)
                                                                             .Query(q => q.Bool(b => b.Must(m => m.Term(FieldName.HouseID, houseId)))));
                }
                catch
                {
                    System.Threading.Thread.Sleep(TimeSpan.FromMilliseconds(new Random().Next(2000, 4000)));
                }

                x++;
            } while (x < 5 && ((SearchResponse == null) || !SearchResponse.ApiCall.Success));
            if ((SearchResponse == null) || !SearchResponse.ApiCall.Success)
            {

                SearchResponse = Client.Search<Data>(s => s.Size(1000)
                                                                             .Query(q => q.Bool(b => b.Must(m => m.Term(FieldName.HouseID, houseId)))));
            }
            return ((SearchResponse == null) || !SearchResponse.ApiCall.Success || (SearchResponse.Documents == null)) ? null : SearchResponse.Documents.ToList<Data>();
        }
    }
}
