﻿using Elasticsearch.Net;
using Nest;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;

namespace Data.ES
{
    public class IndexBase
    {
        #region Class Data
        private string _Index;
        private string _TypeName;
        private ElasticClient _Client;

        protected ElasticClient Client { get { return _Client; } }
        protected string Index { get { return _Index; } }
        #endregion

        #region Constructor
        protected IndexBase(string DefaultIndexName, string IndexNameOverride, string TypeName)
        {
            //
            // Figure out the actual index name
            //
            _TypeName = TypeName;
            _Index = String.IsNullOrWhiteSpace(IndexNameOverride) ? DefaultIndexName : IndexNameOverride;

            //
            // Set the connection to the cluster
            //
            //string UserName = ConfigurationManager.AppSettings["ElasticUserName"];
            //string Password = ConfigurationManager.AppSettings["ElasticPassword"];
            
            Uri uri = new Uri("http://localhost:9200");
            ConnectionSettings ConnectionSettings = new ConnectionSettings(uri);

            ConnectionSettings.DefaultIndex(_Index);

            _Client = new ElasticClient(ConnectionSettings);
        }
        #endregion

        #region Public Stuff
        public string GenerateId(string Starter)
        {
            return String.IsNullOrEmpty(Starter) ? Guid.NewGuid().ToString().ToLower().Replace("-", "")
                                                 : String.Format("{0}:{1}", Starter, Strings.GenerateKey(12)).Replace(" ", "");
        }

        public bool BulkSave<T>(List<T> Documents) where T : CommonData
        {
            int Size = 1000;
            int Start = 0;
            for (;;)
            {
                List<T> Items = Documents.Skip(Start * Size).Take(Size).ToList();

                if ((Items != null) && (Items.Count > 0))
                {
                    IBulkResponse IBulkResponse = null;
                    BulkDescriptor BulkDescriptor = new BulkDescriptor();

                    BulkDescriptor.IndexMany<T>(Items);
                    try
                    {
                        IBulkResponse = Client.Bulk(BulkDescriptor);
                    }
                    catch
                    {
                        //just so that we don't bubble up the exception -- we want to fall into the retry logic for a timeout
                        //and i hate empty catch blocks
                    }
                    if ((IBulkResponse == null))// || !IBulkResponse.ApiCall.Success)
                    {
                        //retry up to 5 times at random 2-4 second intervals.
                        for (int x = 1; x <= 5; x++)
                        {
                            System.Threading.Thread.Sleep(TimeSpan.FromMilliseconds(new Random().Next(2000, 4000)));
                            BulkDescriptor = new BulkDescriptor();
                            BulkDescriptor.IndexMany<T>(Items);
                            try
                            {
                                IBulkResponse = Client.Bulk(BulkDescriptor);
                            }
                            catch (Exception ex)
                            {
                                if (x == 5) throw new Exception("Exception after 5 retries for bulk insert", ex);
                            }
                            if ((IBulkResponse == null))// || !IBulkResponse.ApiCall.Success)
                            {
                                if (x == 5) return false;
                            }
                            //else if (IBulkResponse.ApiCall.Success)
                            //{
                            //    break;
                            //}
                        }
                    }

                    Start++;
                }
                else
                {
                    return true;
                }
            }
        }
        #endregion

        #region Helper Classes
        [Serializable]
        public class AccountData : CommonData
        {
            public AccountData() : base()
            {
            }

            [Number(NumberType.Integer, Name = FieldNameBase.AccountID)]
            public int AccountID { get; set; }
            [Number(NumberType.Integer, Name = FieldNameBase.AccountPlatformID)]
            public int AccountPlatformID { get; set; }
            [Keyword(Name = FieldNameBase.Account)]
            public string Account { get; set; }
            [Keyword(Name = FieldNameBase.Platform)]
            public string Platform { get; set; }
        }

        [Serializable]
        public class CommonData
        {
            public CommonData()
            {
                IndexName = null;
                IgnoreCommonFieldCase = false;
            }

            [Keyword(Name = FieldNameBase.Id)]
            public string Id { get; set; }
            [Date(Name = FieldNameBase.CreateDT)]
            public DateTime CreateDT { get; set; }
            [Keyword(Name = FieldNameBase.CreateUser)]
            public string CreateUser { get; set; }
            [Date(Name = FieldNameBase.UpdateDT)]
            public DateTime UpdateDT { get; set; }
            [Keyword(Name = FieldNameBase.UpdateUser)]
            public string UpdateUser { get; set; }

            [Keyword(Ignore = true)]
            public string IndexName { get; set; }
            [Boolean(Ignore = true)]
            public bool IgnoreCommonFieldCase { get; set; }
        }

        public class FieldNameBase : CommonFieldsBase
        {
            public const string AccountID = "accountID";
            public const string Account = "account";
            public const string AccountPlatformIDs = "accountPlatformIDs";
            public const string Platforms = "platforms";
            public const string AccountPlatformID = "accountPlatformID";
            public const string Platform = "platform";
            public const string AccountName = "accountName";
            public const string PlatformName = "platformName";
        }

        public class CommonFieldsBase
        {
            public const string Id = "Id";
            public const string IndexId = "_id";
            public const string IndexScore = "score";
            public const string CreateDT = "createDT";
            public const string CreateUser = "createUser";
            public const string UpdateDT = "updateDT";
            public const string UpdateUser = "updateUser";
        }

        protected static class IndexConfigurations
        {
            public const string Cache = "EsIndex_Cache";
            public const string Pando = "EsIndex_Pando";
            public const string IntelligentMarketing = "EsIndex_IntelligentMarketing";
            public const string BackgroundTask = "EsIndex_BackgroundOperation";
            public const string SignalsActivity_Write = "EsIndex_SignalsActivity_Write";
            public const string SignalsActivity_Read = "EsIndex_SignalsActivity_Read";
            public const string AAMaster = "EsIndex_AAMaster";
            public const string Acxiom = "EsIndex_Acxiom";
            public const string ContentFuze = "EsIndex_ContentFuze";
            public const string Calendar = "EsIndex_Calendar";
            public const string UserDiagnostics = "EsIndex_UserDiagnostics";
            public const string Social = "EsIndex_Social";
            public const string ProfileFuze = "EsIndex_ProfileFuze";
            public const string LandingPage = "EsIndex_LandingPage";
            public const string Core = "EsIndex_Core";
            public const string RoiCampaign = "EsIndex_RoiCampaign";
            public const string RoiVehicle = "EsIndex_RoiVehicle";
            public const string FuzeChat = "EsIndex_FuzeChat";
            public const string Settings = "EsIndex_Settings";
            public const string CustomerPortal = "EsIndex_CustomerPortal";
            public const string Recommended = "EsIndex_Recommended";
            public const string FuzeCast = "EsIndex_FuzeCast";
            public const string Curator = "EsIndex_Curator";
            public const string CRM = "EsIndex_CRM";
            public const string Signals = "EsIndex_Signals";
        }
        #endregion

        #region Protected Stuff
        protected void ValidateRouting(AccountData AccountData)
        {
            if (AccountData.AccountID < 1)
            {
                throw new Exception(String.Format("{0}.{1}.AccountID cannot be empty", _Index, _TypeName, AccountData.AccountID));
            }

            if (String.IsNullOrEmpty(AccountData.Id))
            {
                AccountData.Id = GenerateId(AccountData.AccountID.ToString());
            }
        }

        protected void SetCommons(CommonData CommonData)
        {
            CommonData.CreateDT = (CommonData.CreateDT == DateTime.MinValue) ? DateTime.Now : CommonData.CreateDT;
            CommonData.UpdateDT = DateTime.Now;

            CommonData.CreateUser = String.IsNullOrEmpty(CommonData.CreateUser) ? Environment.MachineName : (CommonData.IgnoreCommonFieldCase ? CommonData.CreateUser : CommonData.CreateUser.ToLower());
            CommonData.UpdateUser = String.IsNullOrEmpty(CommonData.UpdateUser) ? Environment.MachineName : (CommonData.IgnoreCommonFieldCase ? CommonData.UpdateUser : CommonData.UpdateUser.ToLower());
        }

        protected void CheckSaveError(string Id, IIndexResponse IndexResponse)
        {
            string Msg = null;

            if (!IndexResponse.ApiCall.Success) Msg = String.Format("StatusCode \"{0}\" was generated while saving the following {1}.{2}.{3}\r\n{4}", IndexResponse.ApiCall.HttpStatusCode.Value, _Index, _TypeName, Id, IndexResponse.ApiCall.OriginalException);
            if (IndexResponse.ServerError != null) Msg = String.Format("There was a ServerError of {0} was generated saving the following {1}.{2}.{3}", IndexResponse.ServerError, _Index, _TypeName, Id);

            if (Msg != null)
            {
                throw new Exception(Msg);
            }
        }
        #endregion
    }
}