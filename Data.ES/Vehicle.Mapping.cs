﻿using Nest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.ES
{
    public partial class Vehicle
    {

        private void CreateMapping()
        {
            Client.Map<Data>(m => m.Type(FieldName.IndexTypeName)
                                   .Index(Indices.Index(Index))
                                   .Dynamic(DynamicMapping.Strict)
                                   .DateDetection(true)
                                   .NumericDetection(true)
                                   .AutoMap());
        }

        [ElasticsearchType(Name = FieldName.IndexTypeName, IdProperty = FieldName.Id)]
        [Serializable]
        public partial class Data : CommonData
        {
            [Text(Name = FieldName.NickName)]                                       public string NickName { get; set; }
            [Text(Name = FieldName.Make)]                                           public string Make { get; set; }
            [Text(Name = FieldName.Model)]                                          public string Model { get; set; }
            [Number(NumberType.Integer, Name = FieldName.Year)]                     public int Year { get; set; }
            [Text(Name = FieldName.LeanHolder)]                                     public string LeanHolder { get; set; }
            [Number(NumberType.Double, Name = FieldName.MonthlyPayment)]            public double MonthlyPayment { get; set; }
            [Number(NumberType.Double, Name = FieldName.PayoffAmmount)]             public double PayoffAmmount { get; set; }
            [Text(Name = FieldName.VehiclePictureUrl)]                              public string VehiclePictureUrl { get; set; }
            [Number(NumberType.Integer, Name = FieldName.Mileage)]                  public int Mileage { get; set; }
            [Text(Name = FieldName.VIN)]                                            public string VIN { get; set; }
            [Boolean(Name = FieldName.Primary)]                                     public bool Primary { get; set; }

            [Text(Name = FieldName.FirstName)]                                            public string FirstName { get; set; }
            [Text(Name = FieldName.LastName)]                                      public string LastName { get; set; }
            [Text(Name = FieldName.Email)]            public string Email { get; set; }
            [Text(Name = FieldName.PreferedProvider)]            public string PreferedProvider { get; set; }
            [Text(Name = FieldName.ServiceManager)]            public string ServiceManager { get; set; }
            [Text(Name = FieldName.LastServiceDate)]            public string LastServiceDate { get; set; }
            [Text(Name = FieldName.ServiceType)]            public string ServiceType { get; set; }
            [Text(Name = FieldName.Style)]            public string Style { get; set; }
            [Text(Name = FieldName.LastPaymentDate)]            public string LastPaymentDate { get; set; }
            [Text(Name = FieldName.NextServiceDue)]            public string NextServiceDue { get; set; }
            [Text(Name = FieldName.YearToDateServicePaid)]            public string YearToDateServicePaid { get; set; }
            [Text(Name = FieldName.CurrentMPG)]            public string CurrentMPG { get; set; }
            [Text(Name = FieldName.OriginalMPG)]            public string OriginalMPG { get; set; }







            [Keyword(Name = FieldName.VehicleID)]                                   public Guid VehicleID { get; set; }
            [Keyword(Name = FieldName.PersonID)]                                    public Guid PersonID { get; set; }

            [Keyword(Name = FieldName.HouseID)]                                     public Guid HouseID { get; set; }


        }
        public class FieldName : FieldNameBase
        {
            public const string IndexTypeName = Constants.Vehicle;
            public const string NickName = "nickName";
            public const string Make = "make";
            public const string Model = "model";
            public const string Year = "year";
            public const string VehiclePictureUrl = "vehiclePictureUrl";
            public const string VehicleID = "vehicleID";
            public const string PersonID = "personID";
            public const string LeanHolder = "leanHolder";
            public const string LoanAccount = "loanAccount";
            public const string MonthlyPayment = "monthlyPayment";
            public const string PayoffAmmount = "payoffAmmount";
            public const string Mileage = "mileage";
            public const string HouseID = "houseID";
            public const string VIN = "vin";
            public const string RemainingPayments = "remainingPayments";
            public const string NextPayment = "nextPayment";
            public const string PreferedProvider = "preferedProvider";
            public const string ProviderPhone = "providerPhone";
            public const string ServiceManager = "serviceManager";
            public const string LastServiceDate = "lastServiceDate";
            public const string ServiceType = "serviceType";
            public const string Primary = "primary";
            public const string FirstName = "firstName";
            public const string LastName = "lastName";
            public const string Email = "email";
            public const string Style = "style";
            public const string LastPaymentDate = "lastPaymentDate";
            public const string NextServiceDue = "nextServiceDue";
            public const string YearToDateServicePaid = "yearToDateServicePaid";
            public const string CurrentMPG = "currentMPG";
            public const string OriginalMPG = "originalMPG";

        }
    }
}
