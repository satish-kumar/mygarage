﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.ES
{
    public class Constants
    {
        public const string Person = "person";
        public const string Vehicle = "vehicle";
        public const string Offer = "offer";
        public const string Insurance = "insurance";
    }
}
