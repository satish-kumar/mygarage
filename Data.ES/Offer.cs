﻿using Nest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.ES
{
    public partial class Offer : MyGarageIndex
    {

        #region Constructor
        public Offer(bool UpsertMapping = false, string IndexNameOverride = null) : base("offer", IndexNameOverride, Constants.Vehicle)
        {
            if (UpsertMapping)
            {
                CreateMapping();
            }
        }

        #endregion

        //#region Public Stuff
        public bool Delete(string Id)
        {
            IDeleteResponse IDeleteResponse = Client.Delete<Data>(Id);

            return ((IDeleteResponse == null) || !IDeleteResponse.ApiCall.Success) ? false : true;
        }

        public Data Get(string Id)
        {
            IGetResponse<Data> GetResponse = Client.Get<Data>(new DocumentPath<Data>(Id));

            return ((GetResponse == null) || !GetResponse.ApiCall.Success || !GetResponse.Found) ? null : GetResponse.Source;
        }

        public Data Save(Data Data)
        {
            //
            // Set Data items
            //
            SetCommons(Data);

            if (String.IsNullOrWhiteSpace(Data.Id)) Data.Id = GenerateId(null);

            //
            // Do actual ES indexing and return the outcome
            //
            Nest.IndexRequest<Data> IndexRequest = new IndexRequest<Data>(Data, "offer", Constants.Vehicle);
            IIndexResponse IndexResponse = Client.Index<Data>(IndexRequest);

            CheckSaveError(Data.Id, IndexResponse);

            return Data;
        }



        public List<Data> GetByVehicleId(Guid vehicleId)
        {
            ISearchResponse<Data> SearchResponse = null;
            int x = 0;
            do
            {
                try
                {
                    SearchResponse = Client.Search<Data>(s => s.Size(1)
                                                                             .Query(q => q.Bool(b => b.Must(m => m.Term(FieldName.VehicleID, vehicleId)))));
                }
                catch
                {
                    System.Threading.Thread.Sleep(TimeSpan.FromMilliseconds(new Random().Next(2000, 4000)));
                }

                x++;
            } while (x < 5 && ((SearchResponse == null) || !SearchResponse.ApiCall.Success));
            if ((SearchResponse == null) || !SearchResponse.ApiCall.Success)
            {

                SearchResponse = Client.Search<Data>(s => s.Size(1)
                                                                             .Query(q => q.Bool(b => b.Must(m => m.Term(FieldName.VehicleID, vehicleId)))));
            }
            return ((SearchResponse == null) || !SearchResponse.ApiCall.Success || (SearchResponse.Documents == null)) ? null : SearchResponse.Documents.ToList<Data>();
        }

    }
}
