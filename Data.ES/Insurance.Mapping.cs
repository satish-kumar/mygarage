﻿using Nest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Data.ES.IndexBase;

namespace Data.ES
{
    public partial class Insurance
    {
        private void CreateMapping()
        {
            Client.Map<Data>(m => m.Type(FieldName.IndexTypeName)
                                   .Index(Indices.Index(Index))
                                   .Dynamic(DynamicMapping.Strict)
                                   .DateDetection(true)
                                   .NumericDetection(true)
                                   .AutoMap());
        }

        [ElasticsearchType(Name = FieldName.IndexTypeName, IdProperty = FieldName.Id)]
        [Serializable]
        public partial class Data : CommonData
        {
            [Text(Name = FieldName.InsuranceCompany)]            public string InsuranceCompany { get; set; }
            [Text(Name = FieldName.InsurancePolicyNo)]            public string InsurancePolicyNo { get; set; }
            [Text(Name = FieldName.InsuranceCardUrl)]            public string InsuranceCardUrl { get; set; }
            [Text(Name = FieldName.AgentPhone)]                         public string AgentPhone { get; set; }
            [Number(NumberType.Integer, Name = FieldName.Accidents)]            public int Accidents { get; set; }
            [Number(NumberType.Double, Name = FieldName.InsurancePayment)]            public double InsurancePayment { get; set; }




            [Keyword(Name = FieldName.VehicleID)]               public Guid VehicleID { get; set; }
            [Keyword(Name = FieldName.PersonID)]                public Guid PersonID { get; set; }

            [Keyword(Name = FieldName.HouseID)]                 public Guid HouseID { get; set; }
            [Keyword(Name = FieldName.InsuranceID)]            public Guid InsuranceID { get; set; }


        }
        public class FieldName : FieldNameBase
        {
            public const string IndexTypeName = Constants.Vehicle;
            public const string InsuranceCardUrl = "insuranceCardUrl";
            public const string VehicleID = "vehicleID";
            public const string PersonID = "personID";
            public const string InsuranceCompany = "insuranceCompany";
            public const string InsurancePolicyNo = "insurancePolicyNo";
            public const string HouseID = "houseId";
            public const string AgentPhone = "agentPhone";
            public const string Accidents = "accidents";
            public const string NextPaymentDate = "nextPaymentDate";
            public const string InsuranceID = "insuranceId";
            public const string InsurancePayment = "insurancePayment";



        }
    }
}
