﻿using Nest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Data.ES.IndexBase;

namespace Data.ES
{
    public partial class Person
    {

        private void CreateMapping()
        {
            Client.Map<Data>(m => m.Type(FieldName.IndexTypeName)
                                   .Index(Indices.Index(Index))
                                   .Dynamic(DynamicMapping.Strict)
                                   .DateDetection(true)
                                   .NumericDetection(true)
                                   .AutoMap());
        }

        [ElasticsearchType(Name = FieldName.IndexTypeName, IdProperty = FieldName.Id)]
        [Serializable]
        public partial class Data : CommonData
        {
            [Text(Name = FieldName.FirstName)]						public string FirstName { get; set; }
            [Text(Name = FieldName.LastName)]						public string LastName { get; set; }
            [Text(Name = FieldName.ProfilePictureUrl)]				public string ProfilePictureUrl { get; set; }
            [Text(Name = FieldName.Role)]	            public string Role { get; set; }
            [Text(Name = FieldName.Email)]            public string Email { get; set; }
            [Text(Name = FieldName.Phone)]            public string Phone { get; set; }
            [Text(Name = FieldName.Address)]            public string Address { get; set; }


            [Keyword(Name = FieldName.PersonID)]					public Guid PersonID { get; set; }
            [Keyword(Name = FieldName.HouseID)]						public Guid HouseID { get; set; }



        }
        public class FieldName : FieldNameBase
        {
            public const string IndexTypeName = "person";
            public const string FirstName = "firstName";
            public const string LastName = "lastName";
            public const string ProfilePictureUrl = "profilePictureUrl";
            public const string PersonID = "personID";
            public const string HouseID = "houseID";
            public const string Role = "role";
            public const string Address = "address";
            public const string Phone = "phone";
            public const string Email = "email";


        }
    }
}
