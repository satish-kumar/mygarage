﻿using Nest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Data.ES
{
    public class MyGarageIndex : IndexBase
    {
        protected MyGarageIndex(string DefaultIndexName, string IndexNameOverride, string TypeName) : base(DefaultIndexName, IndexNameOverride, TypeName)
        {
            ConnectionSettings connectionSettings = this.Client.ConnectionSettings as ConnectionSettings;

            connectionSettings?.OnRequestCompleted(handler =>
            {
                if (handler.Success || handler.HttpStatusCode == (int)HttpStatusCode.NotFound)
                {
                    return;
                }

                throw new Exception(
                        "Elasticsearch error",
                        new Exception(
                                "Elastic exception response: " + (handler.ResponseBodyInBytes != null ? Encoding.Default.GetString(handler.ResponseBodyInBytes) : "Empty response")
                                + "\r\nElastic DebugInformation: " + (handler.DebugInformation ?? String.Empty),
                                handler.OriginalException));
            });
        }
    }
}
